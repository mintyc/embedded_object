#ifndef __TESTABLE_GUARD_H__
#define __TESTABLE_GUARD_H__

#include <memory>

class embedded;

class testable
{
public:
	static testable &Instance();
	static testable &Initialise(std::unique_ptr<embedded> object, std::shared_ptr<embedded> referenced);

	bool use_object();
	bool use_referenced();

private:
	testable();
	~testable();

	std::unique_ptr<embedded> _object;
	std::shared_ptr<embedded> _referenced;
};

#endif	// guard