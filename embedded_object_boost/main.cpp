#include <iostream>
#include <memory>
#include <string>

#include "testable.h"

#include "../shared/original.h"
#include "../shared/embedded.h"
#include "../shared/make_unique.h"


int main()
{
	embedded *original_ref = new embedded("original ref");

	{
		std::cout << "* original uninitialised\n";
		original &first = original::Instance();
		first.use_object();
		first.use_referenced();

		std::cout << "* original initialised\n";
		original &second = original::Initialise(original_ref);
		second.use_object();
		second.use_referenced();
	}

	std::shared_ptr<embedded> shared_original_ref(original_ref);
	
	{
		std::cout << "* testable first - uninitialised\n";
		testable &t_first = testable::Instance();
		t_first.use_object();
		t_first.use_referenced();

		std::cout << "* testable second - shared original_ref\n";
		testable &t_second = testable::Initialise(NULL, shared_original_ref);

		t_second.use_object();
		t_second.use_referenced();

		auto second_ref = std::make_shared<embedded>("testable ref");

		std::cout << "* testable third - with revised shared implementation\n";
		testable &t_third = testable::Initialise(NULL, second_ref);
		t_third.use_object();
		t_third.use_referenced();

		std::cout << "* testable second - full implementation\n";
		testable &t_fourth = testable::Initialise(make_unique<embedded>("new object"), second_ref);
		t_fourth.use_object();
		t_fourth.use_referenced();
	}

	std::cout << "* end of main\n";

	return 0;
}