#include "testable.h"

#include "../shared/embedded.h"
#include <iostream>

testable &testable::Instance()
{
	static testable the_instance;
	return the_instance;
}

testable &testable::Initialise(std::unique_ptr<embedded> object, std::shared_ptr<embedded> referenced)
{
	testable &t = testable::Instance();

	if (t._referenced || t._object)
	{
		std::cout << "WARN: testable is re-initialising its embedded and referenced objects\n";
	}

	t._object = std::move(object);
	t._referenced = std::move(referenced);

	return t;
}

bool testable::use_object()
{
	if (!_object)
	{
		std::cout << "testable has no valid object\n";
	}
	return _object && _object->operator()();
}

bool testable::use_referenced()
{
	if (_referenced)
	{
		std::cout << "ref count " << _referenced.use_count() << std::endl;
	}
	else
	{
		std::cout << "testable has no valid reference to shared object\n";
	}

	return _referenced && _referenced->operator()();
}


testable::testable()
{
	std::cout << "testable created\n";
}

testable::~testable()
{
};
