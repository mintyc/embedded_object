#ifndef __TESTABLE_GUARD_H__
#define __TESTABLE_GUARD_H__

class embedded;

class testable
{
public:
	static testable &Instance();
	static testable &Initialise(embedded *object, embedded *referenced);

	bool use_object();
	bool use_referenced();

private:
	testable();
	~testable();

	embedded *_object;
	embedded *_referenced;
};

#endif	// guard