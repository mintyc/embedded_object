#include "testable.h"

#include "../shared/embedded.h"
#include <iostream>

testable &testable::Instance()
{
	static testable the_instance;
	return the_instance;
}

testable &testable::Initialise(embedded *object, embedded *referenced)
{
	testable &t = testable::Instance();

	if (t._referenced || t._object)
	{
		std::cout << "WARN: testable is re-initialising its embedded and referenced objects\n";
	}
	else
	{
		t._object = object;
		t._referenced = referenced;
		return t;
	}

	if (t._object != object)	// avoid self 'assignment'
	{
		delete t._object;
		t._object = object;
	}
	t._referenced = referenced;	// replace but don't delete old 'shared' reference

	return t;
}

bool testable::use_object()
{
	if (!_object)
	{
		std::cout << "testable has no valid object\n";
	}

	return _object && _object->operator()();
}

bool testable::use_referenced()
{
	if (!_referenced)
	{
		std::cout << "testable has no valid reference to shared object\n";
	}
	return _referenced && _referenced->operator()();
}


testable::testable()
{
	std::cout << "testable created\n";
}

testable::~testable()
{
	delete _object;
};
