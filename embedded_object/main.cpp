
#include "testable.h"
#include "../shared/original.h"
#include "../shared/embedded.h"

#include <iostream>
#include <string>

int main()
{
	std::cout << "* original first\n";
	original &first = original::Instance();
	first.use_object();

	std::cout << "* original second\n";
	original &second = original::Instance();
	second.use_object();

	std::cout << "* testable first - uninitialised\n";
	testable &t_first = testable::Instance();
	t_first.use_object();

	std::cout << "* testable second - uninitialised\n";
	testable &t_second = testable::Instance();

	t_first.use_object();
	t_second.use_object();

	embedded first_ref("third");

	std::cout << "* testable third - with implementation\n";
	testable &t_third = testable::Initialise(new embedded(), &first_ref);
	t_first.use_object();
	t_second.use_object();

	embedded *second_ref = new embedded();

	std::cout << "* testable second - alt implementation\n";
	t_second.Initialise(new embedded(), second_ref);
	t_first.use_object();
	t_second.use_object();

	std::cout << "* end of main\n";

	return 0;
}