#include <iostream>
#include <memory>

#include "../shared/embedded.h"

template<typename T>
std::unique_ptr<T> make_unique()
{
	return std::unique_ptr<T>( new T() );
}
 
template<typename T, typename A1>
std::unique_ptr<T> make_unique( A1&& a1 )
{
	return std::unique_ptr<T>( new T( std::forward<A1>(a1) ) );
}
 
template<typename T, typename A1, typename A2>
std::unique_ptr<T> make_unique( A1&& a1, A2&& a2 )
{
	return std::unique_ptr<T>( new T( std::forward<A1>(a1), std::forward<A2>(a2) ) );
}
 
// etc. for as many constructor parameters as you want to support

int main()
{
	std::cout << "* original first\n";
	return 0;
}