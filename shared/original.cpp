#include "original.h"

#include <iostream>

original &original::Instance()
{
	static original the_instance;
	return the_instance;
}

original &original::Initialise(embedded *referenced)
{
	original &t = original::Instance();

	if (NULL != t._referenced)
	{
		std::cout << "WARN: original is re-initialising with its embedded reference\n";
	}

	// NOT owned - so leave old reference owned by caller
	// Ideally we would use shared_ptr here
	t._referenced = referenced;

	return t;
}

bool original::use_object()
{
	return _object();
}

bool original::use_referenced()
{
	if (!_referenced)
	{
		std::cout << "original has no valid reference to shared object\n";
	}
	return _referenced && _referenced->operator()();
}

original::original()
	: _referenced(NULL)
{
	std::cout << "Original created\n";
}

original::~original() {};
