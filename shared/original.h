#ifndef __ORIGINAL_GUARD_H__
#define __ORIGINAL_GUARD_H__

#include "embedded.h"

class original
{
public:
	static original &Instance();

	static original &Initialise(embedded *referenced);

	bool use_object();
	bool use_referenced();

private:
	original();
	~original();

	embedded _object;
	embedded *_referenced;
};

#endif	// guard