#include "embedded.h"

#include <iostream>

bool embedded::operator() (void)
{
	std::cout << "Embedded " << _name << " used\n";
	return true;
}

embedded::~embedded()
{
	std::cout << "Embedded " << _name << " destroyed\n";
}