#ifndef __EMBEDDED_GUARD_H__
#define __EMBEDDED_GUARD_H__

#include <string>

class embedded
{
public:
	bool operator()(void);

	embedded(const std::string &name = "default")
	: _name(name)
	{}

	virtual ~embedded();
protected:
	std::string _name;
};

#endif
